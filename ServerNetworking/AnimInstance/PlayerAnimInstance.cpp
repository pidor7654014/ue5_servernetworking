// Fill out your copyright notice in the Description page of Project Settings.


#include "../AnimInstance/PlayerAnimInstance.h"
#include "../MainPlayer/ServerNetworkingCharacter.h"
#include <Net/UnrealNetwork.h>
#include <Net/Core/PushModel/PushModel.h>

void UPlayerAnimInstance::InitializeAnimation() {
	Super::InitializeAnimation();

	
	Speed = 0.f;
	HeldingWeaponName = "";
	Pitch = 0.0;
}

void UPlayerAnimInstance::UpdateAnimInstanceData() {

	auto pawn = TryGetPawnOwner();
	if (!pawn)
		return;

	auto player = Cast<AServerNetworkingCharacter>(pawn);
	if (!player)
		return;
	 
	Pitch = player->AimRotation.Pitch;
 	Speed = player->GetVelocity().Size();
	HeldingWeaponName = player->GetHeldItemName();
}

 
 
void UPlayerAnimInstance::NativeUpdateAnimation(float Delta) {
	Super::NativeUpdateAnimation(Delta);

	UpdateAnimInstanceData();

}
