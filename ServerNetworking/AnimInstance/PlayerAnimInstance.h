// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "PlayerAnimInstance.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class UPlayerAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AnimInstance")
	float Speed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AnimInstance")
	double Pitch;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AnimInstance")
	FString HeldingWeaponName;

	void UpdateAnimInstanceData();

 	void InitializeAnimation();
	void NativeUpdateAnimation(float Delta);

  };
