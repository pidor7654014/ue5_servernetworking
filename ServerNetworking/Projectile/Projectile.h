// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Projectile.generated.h"

UCLASS(Blueprintable)
class AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
 	AProjectile();

	UProjectileMovementComponent* GetMovement();

	UFUNCTION(Server, Reliable)
	void ServerHitPlayer(AActor* Attacker, AActor* target, FVector End);
	void ServerHitPlayer_Implementation(AActor* Attacker, AActor* target, FVector End);

	UFUNCTION(NetMulticast, Reliable)
	void MultiCastHitPlayer(AActor* Attacker, AActor* target, FVector End);
	void MultiCastHitPlayer_Implementation(AActor* Attacker, AActor* target, FVector End);

	UFUNCTION(Server, Reliable)
	void ServerProjectileUpdate(FVector Location, FVector Velocity, float ProjectileSpeed);
	void ServerProjectileUpdate_Implementation(FVector Location, FVector Velocity, float ProjectileSpeed);


protected:

	
	void ProjectileUpdate();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Projectile")
	float damage;

	UFUNCTION(BlueprintCallable, Category="Projectile")
	void OnProjectileStop(FHitResult impact);
 
 
	UFUNCTION(Server, Reliable)
	void ServerProjectileLaunch(float ProjectileSpeed, float MaxProjectileSpeed, AActor* Attacker);
	void ServerProjectileLaunch_Implementation(float ProjectileSpeed, float MaxProjectileSpeed, AActor* Attacker);

	 
	UFUNCTION(Server, Reliable)
	void ServerDestroyObject(FVector Location, bool debug);
	void ServerDestroyObject_Implementation(FVector Location, bool debug);

	UFUNCTION(NetMulticast, Reliable)
	void MultiCastDestroyObject(FVector Location, bool debug);
	void MultiCastDestroyObject_Implementation(FVector Location, bool debug);

	 

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
