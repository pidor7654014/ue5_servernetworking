// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "../MainPlayer/ServerNetworkingCharacter.h"
#include <Net/UnrealNetwork.h>
#include <Kismet/KismetSystemLibrary.h>
#include "../Utils/ServerSideObject.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AProjectile::AProjectile()
{
	SetReplicates(true);
	SetReplicateMovement(true);
	PrimaryActorTick.bCanEverTick = true;

 	Tags.Add("Projectile");
}

UProjectileMovementComponent* AProjectile::GetMovement() {

	if (!this)
		return nullptr;

	for (auto comp : GetComponents()) {

		if (!comp)
			continue;

		if (comp->GetName().Contains("ProjectileMovement"))
		{
			auto prj = Cast<UProjectileMovementComponent>(comp);
			if (prj != nullptr)
				return prj;
		}
	}

	return nullptr;
}

void AProjectile::ServerProjectileLaunch_Implementation(float ProjectileSpeed, float MaxProjectileSpeed, AActor* Attacker) {

	if (HasAuthority()) {
		auto ServerObj = Cast<AServerSideObject>(UGameplayStatics::GetActorOfClass(GetWorld(), AServerSideObject::StaticClass()));
		if (!ServerObj)
			return;
 
		if (ServerObj->FindProjectileUpdates(this))
			return;

		ServerObj->AddProjectileUpdates(this);
		auto ProjectileUpdates = ServerObj->FindProjectileUpdates(this);
		auto PlayerUpdates = ServerObj->FindPlayerUpdates(Cast<AServerNetworkingCharacter>(ProjectileUpdates->projectile->GetOwner()));
		if (!PlayerUpdates)
			return;

		auto StartPos = PlayerUpdates->ShootPos;
		ProjectileUpdates->StartPos = StartPos;
		ProjectileUpdates->LaunchVelocity = this->GetActorForwardVector() * ProjectileSpeed;
 		ProjectileUpdates->ProjectileSpeed = ProjectileSpeed;
		ProjectileUpdates->MaxProjectileSpeed = MaxProjectileSpeed;
		ProjectileUpdates->Updates.Add(StartPos);
		ProjectileUpdates->prevPos = StartPos;
		ProjectileUpdates->traveledDistance = FVector::Dist(Attacker->GetActorLocation(), StartPos);
		ProjectileUpdates->traveledTime = ProjectileUpdates->traveledDistance / (ProjectileSpeed * 0.01f);
		ProjectileUpdates->ProjectileID = PlayerUpdates->ProjectileID;
	}
}

 void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	
	auto owner = GetOwner();
	if (!owner)
		return;

	auto ProjectileMovement = GetMovement();
	if (!ProjectileMovement)
		return;

	ServerProjectileLaunch(ProjectileMovement->InitialSpeed, ProjectileMovement->MaxSpeed, owner);
}

 
void AProjectile::ProjectileUpdate() {

	if (!this)
		return;

	auto ProjectileMovement = GetMovement();
	if (!ProjectileMovement)
		return;

	ServerProjectileUpdate(GetActorLocation(), ProjectileMovement->Velocity, ProjectileMovement->InitialSpeed);
}

void AProjectile::ServerProjectileUpdate_Implementation(FVector Location, FVector Velocity, float ProjectileSpeed) {

	if (HasAuthority()) {

		auto ServerObj = Cast<AServerSideObject>(UGameplayStatics::GetActorOfClass(GetWorld(), AServerSideObject::StaticClass()));
		if (!ServerObj)
			return;

		auto ProjectileUpdates = ServerObj->FindProjectileUpdates(this);
		auto PlayerUpdates = ServerObj->FindPlayerUpdates(Cast<AServerNetworkingCharacter>(GetOwner()));

		if (Location.IsZero() || Velocity.IsZero())
			return;

		if (!ProjectileUpdates || !PlayerUpdates)
			return;

		auto StartPos = ProjectileUpdates->StartPos;
		if (StartPos.IsZero())
			return;

		int size = ProjectileUpdates->Updates.Num(), MaxSize = 32;
		
		auto projectile_desync = 1.f + ProjectileUpdates->ProjectileSpeed * 0.0001667f;
		float max_projectile_desync = 15.f;
		auto DesyncTime = FMath::Clamp(projectile_desync + PlayerUpdates->LastSentTick + GetWorld()->DeltaTimeSeconds, projectile_desync, max_projectile_desync);

		auto _ProjectileSpeed = ProjectileUpdates->ProjectileSpeed;
		auto ProjectileMinSpeed = projectile_desync * 1000.f;
		auto ProjectileMaxSpeed = ProjectileUpdates->MaxProjectileSpeed;

		if (ProjectileSpeed > ProjectileMaxSpeed || ProjectileSpeed < ProjectileMinSpeed)
			return;
 
		ProjectileUpdates->prevPos = ProjectileUpdates->Updates[size - 1];
		ProjectileUpdates->ProjectileSpeed = ProjectileSpeed;
		ProjectileUpdates->traveledDistance = FVector::Dist(StartPos, ProjectileUpdates->prevPos) * 0.01;
		ProjectileUpdates->traveledTime = ProjectileUpdates->traveledDistance / (ProjectileUpdates->ProjectileSpeed * 0.1f);

		
		auto prevPos = ProjectileUpdates->prevPos;
		auto nextPos = prevPos + Velocity * 0.05f;
		auto NewDist = FVector::Dist(Location, nextPos) * 0.01;
		auto MaxNewStep = projectile_desync + DesyncTime + ProjectileUpdates->traveledDistance * (max_projectile_desync * 0.01f);


 		if (NewDist > MaxNewStep) {
			//GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("Server ProjectileUpdate NextPos %f, MaxStep %f"), NewDist, MaxNewStep));
			return;
		}
 
		FHitResult hit;
		TArray<AActor*> ignoreObjects;
		ignoreObjects.Add(this);
		bool LOS = UKismetSystemLibrary::LineTraceSingle(GetWorld(), Location, prevPos, UEngineTypes::ConvertToTraceType(ECC_Visibility), false, ignoreObjects, EDrawDebugTrace::None, hit, true);
		if (LOS && hit.bBlockingHit) {
			auto actor = hit.GetActor();
			if (actor != nullptr && !actor->ActorHasTag("Player")) {
				GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("Server ProjectileUpdate LOS")));
				return;
			}
		}
		//DrawDebugLine(GetWorld(), Location, prevPos, FColor::Yellow, false, 3.f);

		//for (auto list : ProjectileUpdates->Updates)
		//	DrawDebugSphere(GetWorld(), list, 12.f, 4, FColor::Red, false, 1.f);

		//Trajectory Update
		if (size < MaxSize) {
			ProjectileUpdates->Updates.Add(Location);
			return;
		}

		auto lastPos = ProjectileUpdates->Updates[size - 1];
		ProjectileUpdates->Updates.Empty();
		ProjectileUpdates->Updates.Add(lastPos);
	}

}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
 

	ProjectileUpdate();
}
 

void AProjectile::ServerHitPlayer_Implementation(AActor* Attacker, AActor* target, FVector End) {

	auto ServerObj = Cast<AServerSideObject>(UGameplayStatics::GetActorOfClass(GetWorld(), AServerSideObject::StaticClass()));
	if (!ServerObj) {
		MultiCastDestroyObject(End, false);
		return;
	}

	auto _ProjectileUpdates = ServerObj->FindProjectileUpdates(this);
	if (!_ProjectileUpdates) {
		MultiCastDestroyObject(End, false);
		return;
	}

	if (!Attacker || !target) {
		MultiCastDestroyObject(End, false);
		return;
	}

	if (target == Attacker) {
		MultiCastDestroyObject(End, false);
		return;
	}

	auto Start = _ProjectileUpdates->StartPos;
	if (Start.IsZero() || End.IsZero()) {
		MultiCastDestroyObject(End, false);
		return;
	}

	float projectile_desync = 1.f + _ProjectileUpdates->ProjectileSpeed * 0.0001667f;
	float max_projectile_desync = 15.f;
	auto player = Cast<AServerNetworkingCharacter>(Attacker);
	GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("Server projectile_desync %f"), projectile_desync));

	auto PlayerUpdates = ServerObj->FindPlayerUpdates(player);
	if (!PlayerUpdates) {
		MultiCastDestroyObject(End, false);
		return;
	}

	if (_ProjectileUpdates->ProjectileID != PlayerUpdates->ProjectileID) {
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, FString::Printf(TEXT("Projectile ID Wrong %d player %d"), _ProjectileUpdates->ProjectileID, PlayerUpdates->ProjectileID));
		MultiCastDestroyObject(End, false);
		return;
	}

	auto DesyncTime = FMath::Clamp(projectile_desync + PlayerUpdates->LastSentTick + GetWorld()->DeltaTimeSeconds, projectile_desync, max_projectile_desync);
 	auto TargetDistance = FVector::Dist(Start, target->GetActorLocation()) * 0.01;
	auto ProjectileTravelDistance = _ProjectileUpdates->traveledDistance;
	auto MinTravelToTarget = TargetDistance > max_projectile_desync + DesyncTime ? FMath::Clamp(TargetDistance - (DesyncTime * 2.f), projectile_desync, 99999.0) : 0.0;

 
	if (ProjectileTravelDistance < MinTravelToTarget) {
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("ProjectileTravelDistance %f, MinTravelToTarget %f"), ProjectileTravelDistance, MinTravelToTarget));
		MultiCastDestroyObject(GetActorLocation(), false);
		return;
	}

	/*auto CurrentTravelTime = DesyncTime * 0.007f + _ProjectileUpdates->traveledTime * max_projectile_desync;
	auto EndTravelTime = (TargetDistance / (_ProjectileUpdates->ProjectileSpeed * 0.01f)) - ((projectile_desync - DesyncTime) * 0.0000001f);
	if (CurrentTravelTime < EndTravelTime) {
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("traveledTime %f, EndTravelTime %f"), CurrentTravelTime, EndTravelTime));
		MultiCastDestroyObject(GetActorLocation(), false);
		return;
	}
	GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("traveledTime %f, EndTravelTime %f"), CurrentTravelTime, EndTravelTime));
	*/

	bool flag = true, flag2 = false, flag3 = true, flag4 = false;
 
	auto DistEnd = FVector::Dist(_ProjectileUpdates->prevPos, End) * 0.01;
	if (DistEnd > projectile_desync + max_projectile_desync)
		flag = false;

	if (!flag) {
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, TEXT("Server trajectory flag"));
		MultiCastDestroyObject(End, false);
		return;
	}

	FHitResult hit;
 	TArray<AActor*> ignoreObjects;
	ignoreObjects.Add(this);
	ignoreObjects.Add(Attacker);
	bool LOS = UKismetSystemLibrary::LineTraceSingle(GetWorld(), _ProjectileUpdates->prevPos, target->GetActorLocation(), UEngineTypes::ConvertToTraceType(ECC_Visibility), false, ignoreObjects, EDrawDebugTrace::None, hit, true);
	if (LOS && hit.bBlockingHit) {
		auto actor = hit.GetActor();
		if (actor != nullptr && actor->ActorHasTag("Player")) {
			flag2 = true;
		}
	}

	if (!flag2) {
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, TEXT("Server trajectory LOS flag"));
		MultiCastDestroyObject(End, false);
		return;
	}

	FPredictProjectilePathParams predict;
	predict.StartLocation = Start;
	predict.LaunchVelocity = _ProjectileUpdates->LaunchVelocity;
	predict.ProjectileRadius = 0.1f;
	predict.SimFrequency = 50.f;
	predict.MaxSimTime = max_projectile_desync;
	predict.OverrideGravityZ = 0.f;
	predict.bTraceWithCollision = true;
 	predict.TraceChannel = ECC_Visibility;
	predict.DrawDebugTime = 0.f;
	predict.DrawDebugType = EDrawDebugTrace::None;
  	predict.ActorsToIgnore = ignoreObjects;

	FPredictProjectilePathResult predict_result;
	if (UGameplayStatics::PredictProjectilePath(GetWorld(), predict, predict_result)) {
		auto DesyncPrediction = 2.f + DesyncTime * 0.1f;
 
		for (auto &path : predict_result.PathData) {

			if (path.Location.IsZero())
				continue;

			auto dist = FVector::Dist(path.Location, End) * 0.01;
			if (dist <= DesyncPrediction) {
				flag4 = true;
				//GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("Server dista %f, DesyncPrediction %f"), dist, DesyncPrediction));
				break;
			}

			//DrawDebugSphere(GetWorld(), path.Location, 12.f, 4, FColor::Red, false, 1.f);
		}
	}

	if (!flag4) {
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, TEXT("Server Predict trajectory flag"));
		MultiCastDestroyObject(End, false);
		return;
	}




//	DrawDebugSphere(GetWorld(), _ProjectileUpdates->prevPos, 12.f, 4, FColor::Red, false, 1.f);


	GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Blue, TEXT("Server Hit Valid"));
	//DrawDebugSphere(GetWorld(), End, 5.f, 4, FColor::Green, false, 3.f);
	//DrawDebugLine(GetWorld(), Start, End, FColor::Yellow, false, 3.f);
	//DrawDebugSphere(GetWorld(), Start, 15.f, 4, FColor::Blue, false, 3.f);
	GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("Server desyncTime %f"), DesyncTime));

	ServerObj->ClearProjectileUpdates(this);
  	MultiCastHitPlayer(Attacker, target, End);
}

void AProjectile::MultiCastHitPlayer_Implementation(AActor* Attacker, AActor* target, FVector End) {


	auto player = Cast<AServerNetworkingCharacter>(target);
 	if (player != nullptr) {
		player->Health -= 10.f;
	}

	Destroy();
}

void AProjectile::ServerDestroyObject_Implementation(FVector Location, bool debug) {
	MultiCastDestroyObject(Location, debug);
}

void AProjectile::MultiCastDestroyObject_Implementation(FVector Location, bool debug) {

	if (debug) {
		//GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Yellow, TEXT("Projectile Destroyed"));
		DrawDebugSphere(GetWorld(), Location, 5.f, 4, FColor::Yellow, false, 3.f);
	}
	//UE_LOG(LogTemp, Log, TEXT("Projectile Destroyed"));
	Destroy();
}

void AProjectile::OnProjectileStop(FHitResult impact) {

	if (!GetWorld())
		return;

	//UE_LOG(LogTemp, Log, TEXT("Projectile Stoped"));
	if (!impact.bBlockingHit)
		return;

	auto actor = impact.GetActor();
	if (!actor || !actor->IsA(AServerNetworkingCharacter::StaticClass())) {
		ServerDestroyObject(impact.Location, true);
		return;
	}


	ServerHitPlayer(GetOwner(), actor, impact.Location);
}


void AProjectile::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {

	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

  
}