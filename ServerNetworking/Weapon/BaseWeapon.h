// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseWeapon.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class ABaseWeapon : public AActor
{
	GENERATED_BODY()

public:
	ABaseWeapon();
	~ABaseWeapon();

	void Reload(UAnimInstance* anim);
	void Shoot(UAnimInstance* anim);
	void SetAmmo(int value);
	int GetAmmo(bool max);
	int GetAmmoType();
	float GetReloadTime();
	FString GetWeaponName();
	bool IsPrimary();
 	USkeletalMeshComponent* GetMesh();
	UClass* GetProjectile();
	FName GetSocket();

	UFUNCTION(Server, Reliable)
	void ServerUpdateAmmo(int value, bool flag);
	void ServerUpdateAmmo_Implementation(int value, bool flag);

	UFUNCTION(NetMulticast, Reliable)
	void MultiCastUpdateAmmo(int value);
	void MultiCastUpdateAmmo_Implementation(int value);

	UFUNCTION(Server, Reliable)
	void ServerPushWeapon(AActor* Player);
	void ServerPushWeapon_Implementation(AActor* Player);

	UFUNCTION(Server, Reliable)
	void ServerReUpdateAmmo(AActor* Player, int ammo);
	void ServerReUpdateAmmo_Implementation(AActor* Player, int ammo);

protected:
 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseWeapon")
	class USkeletalMeshComponent* Mesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseWeapon")
	float recoil;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseWeapon")
	UClass* Projectile;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseWeapon")
	UAnimationAsset* ReloadAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseWeapon")
	UAnimationAsset* ShootAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseWeapon")
	UAnimMontage* FPS_ReloadAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseWeapon")
	UAnimMontage* FPS_ShootAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseWeapon")
	int AmmoType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseWeapon")
	FString Name;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "BaseWeapon")
	int Ammo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseWeapon")
	int MaxAmmo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseWeapon")
	bool IsPrimaryWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseWeapon")
	FName _socket;

  	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
};
