// Fill out your copyright notice in the Description page of Project Settings.


#include "../Weapon/BaseWeapon.h"
#include <Net/UnrealNetwork.h>
#include "Kismet/GameplayStatics.h"
#include "../Utils/ServerSideObject.h"

ABaseWeapon::ABaseWeapon()
{
	SetReplicates(true);
	SetReplicateMovement(true);

	auto root = CreateDefaultSubobject<USceneComponent>("Root");
	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>("GunMesh");
	Mesh->bEditableWhenInherited = true;
	Mesh->SetupAttachment(root);
}

ABaseWeapon::~ABaseWeapon()
{
	
}

float ABaseWeapon::GetReloadTime() {

	if (!this || !FPS_ReloadAnim)
		return 0.f;

	return FPS_ReloadAnim->GetPlayLength();
}

void ABaseWeapon::Reload(UAnimInstance* anim) {

	if (!this || !Mesh || !anim || !ReloadAnim || !FPS_ReloadAnim)
		return;

	anim->Montage_Play(FPS_ReloadAnim);
	Mesh->PlayAnimation(ReloadAnim, false);
}

void ABaseWeapon::Shoot(UAnimInstance* anim) {

	if (!this || !Mesh || !anim || !ShootAnim || !FPS_ShootAnim)
		return;

	anim->Montage_Play(FPS_ShootAnim);
	Mesh->PlayAnimation(ShootAnim, false);
 }

void ABaseWeapon::SetAmmo(int value) {

	if (!this)
		return;

 	Ammo = value;
}

int ABaseWeapon::GetAmmo(bool max) {

	if (!this)
		return 0;

	return max ? MaxAmmo : Ammo;
}

int ABaseWeapon::GetAmmoType() {
	
	if (!this)
		return 0;

	return AmmoType;
}

FString ABaseWeapon::GetWeaponName() {

	if (!this)
		return "";

	return Name;
}

bool ABaseWeapon::IsPrimary() {

	if (!this)
		return false;

	return IsPrimaryWeapon;
}

USkeletalMeshComponent* ABaseWeapon::GetMesh() {

	if (!this)
		return nullptr;

	return Mesh;
}

UClass* ABaseWeapon::GetProjectile() {

	if (!this)
		return nullptr;

	return Projectile;
}

FName ABaseWeapon::GetSocket() {
	
	if (!this)
		return FName("None");

	return _socket;
}

void ABaseWeapon::ServerUpdateAmmo_Implementation(int value, bool flag) {

	if (!this)
		return;
	 
	auto prevAmmo = this->Ammo - value;
	if ((prevAmmo <= 0 || value > this->MaxAmmo) && !flag)
		return;

	GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, TEXT("Ammo Updated Pushed"));

	MultiCastUpdateAmmo(value);
}

void ABaseWeapon::MultiCastUpdateAmmo_Implementation(int value) {

	if (!this)
		return;

	this->Ammo = value;
}

void ABaseWeapon::ServerPushWeapon_Implementation(AActor* Player) {

	if (!this || !Player)
		return;

	auto ServerObj = Cast<AServerSideObject>(UGameplayStatics::GetActorOfClass(GetWorld(), AServerSideObject::StaticClass()));
	if (!ServerObj)
		return;

	auto player = Cast<AServerNetworkingCharacter>(Player);
	if (!player)
		return;

	auto PlayerUpdates = ServerObj->FindPlayerUpdates(player);
	if (!PlayerUpdates)
		return;

	if (PlayerUpdates->WeaponAmmo.Contains(this))
		return;

	PlayerUpdates->WeaponAmmo.Add(this, this->Ammo);
}
 
 
void ABaseWeapon::ServerReUpdateAmmo_Implementation(AActor* Player, int ammo) {

	if (!this || !Player)
		return;

	auto ServerObj = Cast<AServerSideObject>(UGameplayStatics::GetActorOfClass(GetWorld(), AServerSideObject::StaticClass()));
	if (!ServerObj)
		return;

	auto player = Cast<AServerNetworkingCharacter>(Player);
	if (!player)
		return;
 
	auto PlayerUpdates = ServerObj->FindPlayerUpdates(player);
	if (!PlayerUpdates)
		return;
	//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, FString::Printf(TEXT("ClientAmmo")));

	if (!PlayerUpdates->WeaponAmmo.Contains(this))
		return;

	//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, FString::Printf(TEXT("ClientAmmo %d ServerAmmo %d"), ammo, PlayerUpdates->WeaponAmmo[this]));
	
	if (ammo != PlayerUpdates->WeaponAmmo[this]) {
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, TEXT("Reupdated"));
		MultiCastUpdateAmmo(PlayerUpdates->WeaponAmmo[this]);
	}

}

void ABaseWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {

	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(ABaseWeapon, Ammo, COND_None);
}