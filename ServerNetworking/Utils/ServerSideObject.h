// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../MainPlayer/ServerNetworkingCharacter.h"
#include "Projectile.h"
#include "../Weapon/BaseWeapon.h"
#include "ServerSideObject.generated.h"


USTRUCT(BlueprintType)
struct FServerPlayerCharacterStruct
{
	GENERATED_BODY()

public:
	FServerPlayerCharacterStruct();

	AServerNetworkingCharacter* player;
	TMap<ABaseWeapon*, int> WeaponAmmo;
	TMap<int, int> InventoryAmmo;
	float LastSentTime;
 	float LastSentTick;
	FVector Location;
	FVector ShootPos;
	int ProjectileID;
};

USTRUCT(BlueprintType)
struct FServerProjectileStruct
{
	GENERATED_BODY()

public:
	FServerProjectileStruct();

 	AProjectile* projectile;
	TArray<FVector> Updates;
	FVector prevPos;
	FVector StartPos;
	FVector LaunchVelocity;
	float ProjectileSpeed;
	float MaxProjectileSpeed;
	float traveledTime;
	float traveledDistance;
	int ProjectileID;
};

UCLASS(Blueprintable)
class AServerSideObject : public AActor
{
	GENERATED_BODY()
	
public:	
 	AServerSideObject();


	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadWrite)
	TArray<FServerProjectileStruct> ProjectileUpdates;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadWrite)
	TArray<FServerPlayerCharacterStruct> PlayerUpdates;

  	FServerProjectileStruct* FindProjectileUpdates(AProjectile* Key);
	void AddProjectileUpdates(AProjectile* Key);
	void ClearProjectileUpdates(AProjectile* Key);

	FServerPlayerCharacterStruct* FindPlayerUpdates(AServerNetworkingCharacter* Key);
	void AddPlayerUpdates(AServerNetworkingCharacter* Key);

protected:

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

};
