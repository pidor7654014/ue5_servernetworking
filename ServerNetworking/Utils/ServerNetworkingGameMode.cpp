// Copyright Epic Games, Inc. All Rights Reserved.

#include "../Utils/ServerNetworkingGameMode.h"
#include "../MainPlayer/ServerNetworkingCharacter.h"
#include "UObject/ConstructorHelpers.h"

AServerNetworkingGameMode::AServerNetworkingGameMode()
{
	// set default pawn class to our Blueprinted character
	bReplicates = true;
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/MainCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}