// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ServerNetworkingGameMode.generated.h"

UCLASS(minimalapi)
class AServerNetworkingGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AServerNetworkingGameMode();

};



