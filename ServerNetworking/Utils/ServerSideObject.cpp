// Fill out your copyright notice in the Description page of Project Settings.


#include "ServerSideObject.h"
#include <Net/UnrealNetwork.h>


FServerProjectileStruct::FServerProjectileStruct() {
	projectile = nullptr;
	Updates = TArray<FVector>();
    traveledTime = 0.f;
    traveledDistance = 0.f;
    prevPos = FVector::Zero();
    StartPos = FVector::Zero();
    LaunchVelocity = FVector::Zero();
    ProjectileSpeed = 0.f;
    MaxProjectileSpeed = 0.f;
    ProjectileID = 0;
 }

FServerPlayerCharacterStruct::FServerPlayerCharacterStruct() {
    player = nullptr;
    WeaponAmmo = TMap<ABaseWeapon*, int>();
    InventoryAmmo = TMap<int, int>();
    LastSentTime = 0.f;
    LastSentTick = 0.f;
    Location = FVector::Zero();
    ProjectileID = 0;
    ShootPos = FVector::Zero();
}

AServerSideObject::AServerSideObject()
{
 	PrimaryActorTick.bCanEverTick = true;

    PlayerUpdates = TArray<FServerPlayerCharacterStruct>();
	ProjectileUpdates = TArray<FServerProjectileStruct>();
}

void AServerSideObject::BeginPlay()
{
	Super::BeginPlay();
	
}

void AServerSideObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FServerProjectileStruct* AServerSideObject::FindProjectileUpdates(AProjectile* Key)
{
    if (!this)
        return nullptr;

    for (FServerProjectileStruct& Entry : ProjectileUpdates)
    {
        if (Entry.projectile == Key)
        {
            return &Entry;
        }
    }
    return nullptr;
}

void AServerSideObject::AddProjectileUpdates(AProjectile* Key)
{
    if (!this)
        return;

    FServerProjectileStruct NewEntry;
    NewEntry.projectile = Key;
    NewEntry.Updates = TArray<FVector>();
    NewEntry.traveledTime = 0.f;
    NewEntry.traveledDistance = 0.f;
    NewEntry.prevPos = FVector::Zero();
    NewEntry.StartPos = FVector::Zero();
    NewEntry.LaunchVelocity = FVector::Zero();
    NewEntry.ProjectileSpeed = 0.f;
    NewEntry.MaxProjectileSpeed = 0.f;
    NewEntry.ProjectileID = 0;
    ProjectileUpdates.Add(NewEntry);
}

void AServerSideObject::ClearProjectileUpdates(AProjectile* Key) {

    if (!this)
        return;

    for (int i = 0; i < ProjectileUpdates.Num(); i++) {

        if (!ProjectileUpdates.IsValidIndex(i))
            continue;

        if (ProjectileUpdates[i].projectile == Key) 
            ProjectileUpdates.RemoveAt(i);
        
    }
}

FServerPlayerCharacterStruct* AServerSideObject::FindPlayerUpdates(AServerNetworkingCharacter* Key)
{
    if (!this)
        return nullptr;

    for (FServerPlayerCharacterStruct& Entry : PlayerUpdates)
    {
        if (Entry.player == Key)
        {
            return &Entry;
        }
    }
    return nullptr;
}

 

void AServerSideObject::AddPlayerUpdates(AServerNetworkingCharacter* Key)
{
    if (!this)
        return;

    FServerPlayerCharacterStruct NewEntry;
    NewEntry.player = Key;
    NewEntry.WeaponAmmo = TMap<ABaseWeapon*, int>();
    NewEntry.InventoryAmmo = TMap<int, int>();
    NewEntry.LastSentTime = 0.f;
    NewEntry.LastSentTick = 0.f;
    NewEntry.Location = FVector::Zero();
    NewEntry.ProjectileID = 0;
    NewEntry.ShootPos = FVector::Zero();
    PlayerUpdates.Add(NewEntry);
}


void AServerSideObject::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AServerSideObject, ProjectileUpdates);
    DOREPLIFETIME(AServerSideObject, PlayerUpdates);
}