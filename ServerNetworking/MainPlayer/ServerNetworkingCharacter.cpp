// Copyright Epic Games, Inc. All Rights Reserved.

#include "../MainPlayer/ServerNetworkingCharacter.h"
#include "Engine/LocalPlayer.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/PlayerState.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputActionValue.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include <Net/UnrealNetwork.h>
#include "../Projectile/Projectile.h"
#include "../Utils/ServerSideObject.h"


DEFINE_LOG_CATEGORY(LogTemplateCharacter);

//////////////////////////////////////////////////////////////////////////
// AServerNetworkingCharacter

namespace TestExploits {
	bool BulletTeleport = false;
	bool SilentAimTarget = false;
	bool ChangeStartTrajectoryTarget = false;
 }

AServerNetworkingCharacter::AServerNetworkingCharacter()
{
	SetReplicates(true);
	SetReplicatingMovement(true);

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
		
	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 1500.0f, 0.0f); // ...at this rotation rate

	// Note: For faster iteration times these variables, and many more, can be tweaked in the Character Blueprint
	// instead of recompiling to adjust them
	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 4300.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;
	GetCharacterMovement()->BrakingDecelerationFalling = 1500.0f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 0.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named ThirdPersonCharacter (to avoid direct content references in C++)

	SprintPressed = false;
	Inventory = nullptr;
 	Health = 100.f;
	IsReloading = false;
	ClientTickInterval = 0.1f;
	AimRotation = FRotator::ZeroRotator;
  	Tags.Add("Player");
}


 
FTimerHandle SendTickTimer;
 
void AServerNetworkingCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	ServerSendPlayerTick();
	Inventory = NewObject<UPlayerInventory>(this);
	Inventory->Initialize(GetFPSMesh(), "/Game/ThirdPerson/Blueprints/c10_BP.c10_BP_C", this);
	Inventory->Initialize(GetFPSMesh(), "/Game/ThirdPerson/Blueprints/ak74_BP.ak74_BP_C", this);
	Inventory->ServerInitializeAmmo(this);
	GetWorldTimerManager().SetTimer(SendTickTimer, this, &AServerNetworkingCharacter::SendPlayerTick, ClientTickInterval, true);
}

#pragma region Another Staff

ABaseWeapon* AServerNetworkingCharacter::GetHeldItem() {

	if (!this)
		return nullptr;

	if (!Inventory)
		return nullptr;

	return Inventory->GetWeapon();
}

FString AServerNetworkingCharacter::GetHeldItemName() {

	auto weapon = GetHeldItem();
	if (!weapon)
		return "";

	return weapon->GetWeaponName();
}

USkeletalMeshComponent* AServerNetworkingCharacter::GetFPSMesh(bool another) {

	if (!this)
		return nullptr;

	for (auto mesh : GetComponents()) {

		if (!mesh)
			continue;

		if (!mesh->IsA(USkeletalMeshComponent::StaticClass()))
			continue;

		if (mesh->GetName().Contains(another ? "PlayerMesh" : "FPS")) {
			return Cast<USkeletalMeshComponent>(mesh);
		}

	}

	return nullptr;
}


int AServerNetworkingCharacter::GetWeaponAmmo(bool max) {

	auto weapon = GetHeldItem();
	if (!weapon)
		return 0;

	return weapon->GetAmmo(max);
}

int AServerNetworkingCharacter::GetInventoryAmmo() {

	if (!this)
		return 0;

	if (!Inventory)
		return 0;

	return Inventory->GetAmmo();
}


float AServerNetworkingCharacter::GetClientPing(bool realValue) {

	if (!this)
		return 0.f;

	auto _PlayerState = GetPlayerState();
	if (!_PlayerState)
		return 0.f;

	auto ping = _PlayerState->GetPingInMilliseconds();
	const float min_latency = 30.f;

	if (realValue)
		return ping;

	return ping > min_latency ? ping * 5.f : ping;
}

#pragma endregion

#pragma region PlayerMovement

void AServerNetworkingCharacter::HandleWalk() {
	ServerSendSprintInput(false);
}

void AServerNetworkingCharacter::HandleRun() {
	ServerSendSprintInput(true);
}

void AServerNetworkingCharacter::ServerSendSprintInput_Implementation(bool value) {

	if (!this)
		return;

	SprintPressed = value;
}
 
void AServerNetworkingCharacter::ServerUpdateSpeed_Implementation(float speed) {

	if (!this)
		return;
 
	MultiCastUpdateSpeed(speed);
}

void AServerNetworkingCharacter::MultiCastUpdateSpeed_Implementation(float speed) {

	if (!this)
		return;

	auto movement = this->GetCharacterMovement();
	if (!movement)
		return;
 
	movement->MaxWalkSpeed = speed;
}


void AServerNetworkingCharacter::MultiCastUpdatePosition_Implementation(FVector Location) {
	
	if (!this || Location.IsZero())
		return;

	FHitResult hit;
	this->K2_SetActorLocation(Location, false, hit, true);
}

#pragma endregion


#pragma region PlayerWeaponUpdate

void AServerNetworkingCharacter::SelectSlot1() {
	ServerUpdateWeapon(true);
}

void AServerNetworkingCharacter::SelectSlot2() {
	ServerUpdateWeapon(false);
}

void AServerNetworkingCharacter::HideThirdPersonWeapon() {

	auto mesh = GetFPSMesh(true);
	if (!mesh)
		return;

	TArray<USceneComponent*> childrens;
	mesh->GetChildrenComponents(true, childrens);

	for (auto child : childrens) {

		if (!child)
			continue;

		child->SetHiddenInGame(true);
	}
}

void AServerNetworkingCharacter::ServerUpdateWeapon_Implementation(bool PrimarySlot) {

	if (!this)
		return;

	if (!Inventory)
		return;

	if (!Inventory->GetCurrentSlotWeapon(PrimarySlot))
		return;


	MultiCastUpdateWeapon(PrimarySlot);
	ClientUpdateWeapon(PrimarySlot);
}

void AServerNetworkingCharacter::ClientUpdateWeapon_Implementation(bool PrimarySlot) {

	if (!this)
		return;

	if (!Inventory)
		return;

	auto weapon = Inventory->GetCurrentSlotWeapon(PrimarySlot);
	if (!weapon)
		return;

	Inventory->SelectWeapon(weapon, GetFPSMesh(), true);
}

void AServerNetworkingCharacter::MultiCastUpdateWeapon_Implementation(bool PrimarySlot) {

	if (!this)
		return;

	if (!Inventory)
		return;

	auto weapon = Inventory->GetCurrentSlotWeapon(PrimarySlot);
	if (!weapon)
		return;
 

	Inventory->SelectWeapon(weapon, GetFPSMesh(true), false);
}


FTimerHandle ReloadTimer;
void AServerNetworkingCharacter::ServerSetPlayerReloading_Implementation(bool value) {

	if (!this)
		return;

	MultiCastSetPlayerReloading(value);
}

void AServerNetworkingCharacter::MultiCastSetPlayerReloading_Implementation(bool value) {
	IsReloading = value;
}

void AServerNetworkingCharacter::ClearReload() {
	ServerSetPlayerReloading(false);
	GetWorldTimerManager().ClearTimer(ReloadTimer);
}

void AServerNetworkingCharacter::WeaponReload() {

	if (!this || !Inventory)
		return;

	auto fps_mesh = GetFPSMesh();
	if (!fps_mesh)
		return;

	auto AnimInstance = fps_mesh->GetAnimInstance();
	if (!AnimInstance || AnimInstance->IsAnyMontagePlaying())
		return;

	auto weapon = GetHeldItem();
	if (!weapon)
		return;

	auto Ammo = Inventory->GetAmmo();
	auto AmmoClip = weapon->GetAmmo(false);
	auto AmmoClipSize = weapon->GetAmmo(true);

	if (IsReloading || Ammo <= 0 || AmmoClip >= AmmoClipSize)
		return;

	weapon->Reload(fps_mesh->GetAnimInstance());
	ServerSimulateReload(weapon, AmmoClipSize);

	int LoadValue = 0;
	if (AmmoClip > Ammo) {
		LoadValue = FMath::Clamp(AmmoClipSize - AmmoClip, 0, Ammo);
		weapon->ServerUpdateAmmo(AmmoClip - LoadValue, true);
	}
	else {
		LoadValue = AmmoClipSize - AmmoClip;
		weapon->ServerUpdateAmmo(AmmoClipSize, true);
	}
	Inventory->SetAmmo(Ammo - LoadValue);

	ServerSetPlayerReloading(true);
	GetWorldTimerManager().SetTimer(ReloadTimer, this, &AServerNetworkingCharacter::ClearReload, weapon->GetReloadTime(), false);
}

void AServerNetworkingCharacter::ServerSimulateReload_Implementation(ABaseWeapon* weapon, int AmmoClipSize) {

	if (HasAuthority()) {

		auto ServerObj = Cast<AServerSideObject>(UGameplayStatics::GetActorOfClass(GetWorld(), AServerSideObject::StaticClass()));
		if (!ServerObj)
			return;

		auto PlayerUpdates = ServerObj->FindPlayerUpdates(this);
		if (!PlayerUpdates)
			return;

		if (!PlayerUpdates->WeaponAmmo.Contains(weapon) || !PlayerUpdates->InventoryAmmo.Contains(weapon->GetAmmoType()))
			return;

		auto Ammo = PlayerUpdates->InventoryAmmo[weapon->GetAmmoType()];
		auto AmmoClip = PlayerUpdates->WeaponAmmo[weapon];
		if (IsReloading || Ammo <= 0 || AmmoClip >= AmmoClipSize)
			return;


		int LoadValue = 0;
		if (AmmoClip > Ammo) {
			LoadValue = FMath::Clamp(AmmoClipSize - AmmoClip, 0, Ammo);
			PlayerUpdates->WeaponAmmo[weapon] = AmmoClip - LoadValue;
		}
		else {
			LoadValue = AmmoClipSize - AmmoClip;
			PlayerUpdates->WeaponAmmo[weapon] = AmmoClipSize;
		}
		PlayerUpdates->InventoryAmmo[weapon->GetAmmoType()] = Ammo - LoadValue;

	}

}


#pragma endregion


#pragma region PlayerProjectileShoot
void AServerNetworkingCharacter::ProjectileShoot() {

	if (!this || !GetWorld())
		return;

	if (TestExploits::SilentAimTarget || TestExploits::ChangeStartTrajectoryTarget) {
		ProjectileShootExploit();
		return;
	}
 	
	auto fps_mesh = GetFPSMesh();
	if (!fps_mesh)
		return;
	
	auto AnimInstance = fps_mesh->GetAnimInstance();
	if (!AnimInstance)
		return;

	auto weapon = GetHeldItem();
	if (!weapon)
		return;

	auto camera = GetFollowCamera();
	if (!camera)
		return;

	if (IsReloading || weapon->GetAmmo(false) <= 0)
		return;

	auto StartPos = camera->GetComponentLocation() + GetActorForwardVector() * 120.f;
	auto Rotation = camera->GetComponentRotation();

	weapon->Shoot(AnimInstance);
 	ServerProjectileShoot(StartPos, Rotation, weapon);
	weapon->ServerUpdateAmmo(weapon->GetAmmo(false) - 1, false);
}

void AServerNetworkingCharacter::ServerProjectileShoot_Implementation(FVector Start, FRotator Rotation, ABaseWeapon* weapon) {
 
	if (!this || !GetWorld() || Start.IsZero() || !weapon)
		return;

	auto projectile = weapon->GetProjectile();
	if (!projectile)
		return;

	auto ServerObj = Cast<AServerSideObject>(UGameplayStatics::GetActorOfClass(GetWorld(), AServerSideObject::StaticClass()));
	if (!ServerObj)
		return;

	auto PlayerUpdates = ServerObj->FindPlayerUpdates(this);
	if (!PlayerUpdates)
 		return;
	
	auto PlayerShootPos = this->GetFollowCamera()->GetComponentLocation() + GetActorForwardVector();
	if (PlayerShootPos.IsZero())
 		return;
	
	if (!PlayerUpdates->WeaponAmmo.Contains(weapon))
		return;

	if (IsReloading || PlayerUpdates->WeaponAmmo[weapon] <= 0)
		return;

	PlayerUpdates->ShootPos = Start;
	PlayerUpdates->WeaponAmmo[weapon]--;
 
	DrawDebugLine(GetWorld(), this->GetActorLocation(), Start, FColor::Red, false, 3.f);
	float projectile_desync = 1.5f + this->GetVelocity().Length() * 0.01f, max_projectile_desync = 10.f;
	float DesyncTime = FMath::Clamp(projectile_desync + PlayerUpdates->LastSentTick + GetWorld()->DeltaTimeSeconds, projectile_desync, max_projectile_desync);
	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, FString::Printf(TEXT("Server ProjectileShoot Desync %f"), DesyncTime));

	auto shot_dist = FVector::Dist(PlayerShootPos, Start) * 0.01;
	if (shot_dist > DesyncTime) {
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("Server ProjectileShoot Invalid StartPos %f"), shot_dist));
		return;
	}

	FHitResult hit;
	auto shot_los = UKismetSystemLibrary::LineTraceSingle(GetWorld(), Start, PlayerShootPos, UEngineTypes::ConvertToTraceType(ECC_Visibility), true, TArray<AActor*>(), EDrawDebugTrace::None, hit, false);
	if (shot_los && hit.bBlockingHit) {
		auto actor = hit.GetActor();
		if (actor != nullptr && !actor->ActorHasTag("Player")) {
			GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, TEXT("Server ProjectileShoot LOS"));
			return;
		}
	}
 
 	PlayerUpdates->ProjectileID++;
	MultiCastProjectileShoot(Start, Rotation, weapon);
}
 
void AServerNetworkingCharacter::MultiCastProjectileShoot_Implementation(FVector Start, FRotator Rotation, ABaseWeapon* weapon) {


	if (!this || !GetWorld() || Start.IsZero() || !weapon)
		return;

	auto projectile = weapon->GetProjectile();
	if (!projectile)
		return;


	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;
	SpawnParams.Instigator = GetInstigator();

  	GetWorld()->SpawnActor<AProjectile>(projectile, Start, Rotation, SpawnParams);
}

#pragma endregion
 


void AServerNetworkingCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

  	DOREPLIFETIME_CONDITION(AServerNetworkingCharacter, Inventory, COND_None);
	DOREPLIFETIME_CONDITION(AServerNetworkingCharacter, Health, COND_None);
 	DOREPLIFETIME_CONDITION(AServerNetworkingCharacter, SprintPressed, COND_None);
	DOREPLIFETIME_CONDITION(AServerNetworkingCharacter, IsReloading, COND_None);
	DOREPLIFETIME_CONDITION(AServerNetworkingCharacter, AimRotation, COND_None);
}

 
#pragma region PlayerTickUpdate
void AServerNetworkingCharacter::ServerSendPlayerTick_Implementation() {

	if (!this)
		return;

	auto ServerTime = GetWorld()->RealTimeSeconds;

	auto ServerObj = Cast<AServerSideObject>(UGameplayStatics::GetActorOfClass(GetWorld(), AServerSideObject::StaticClass()));
	if (!ServerObj)
		return;

	auto PlayerUpdates = ServerObj->FindPlayerUpdates(this);
	if (!PlayerUpdates) {
		ServerObj->AddPlayerUpdates(this);
		return;
	}

	PlayerUpdates->LastSentTick = (ServerTime - PlayerUpdates->LastSentTime) + this->GetClientPing() * 0.01f;
	PlayerUpdates->LastSentTime = ServerTime;

	if (FMath::IsNaN(PlayerUpdates->LastSentTick))
		PlayerUpdates->LastSentTick = ClientTickInterval;


	//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, FString::Printf(TEXT("Server PlayerTick %f"), PlayerUpdates->LastSentTick));
	auto Location = this->GetActorLocation();
	if (Location.IsZero())
		return;

	auto movement = this->GetCharacterMovement();
	if (!movement)
		return;

	if (PlayerUpdates->Location.IsZero()) {
		PlayerUpdates->Location = Location;
		return;
	}

	float MaxDesyncDistLocation = (PlayerUpdates->LastSentTick * 0.5f +  GetVelocity().Length() * (movement->IsFalling() ? 1.f : 0.001f));
	auto PointDist = FVector::Dist(Location, PlayerUpdates->Location) * 0.01;
	if (PointDist <= MaxDesyncDistLocation) {
		PlayerUpdates->Location = Location;
	}
	else {
		MultiCastUpdatePosition(PlayerUpdates->Location);
	}	

	//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, FString::Printf(TEXT("Server PointDist %f, MaxDesyncDistLocation %f"), PointDist, MaxDesyncDistLocation));
}

 

void AServerNetworkingCharacter::SendPlayerTick() {

	if (!this || !GetWorld())
		return;

	ServerSendPlayerTick();
	ServerUpdateSpeed(SprintPressed ? 600.f : 300.f);
	ServerUpdateAimRotation(GetBaseAimRotation());
	HideThirdPersonWeapon();
	if (GetHeldItem() != nullptr)
		GetHeldItem()->ServerReUpdateAmmo(this, GetHeldItem()->GetAmmo(false));

	GetWorldTimerManager().ClearTimer(SendTickTimer);
}
#pragma endregion

#pragma region UpdatePlayerRotation
void AServerNetworkingCharacter::ServerUpdateAimRotation_Implementation(FRotator rot) {

	if (!this || rot.IsZero())
		return;

	MultiCastUpdateAimRotation(rot);
}

void AServerNetworkingCharacter::MultiCastUpdateAimRotation_Implementation(FRotator rot) {

	if (!this || rot.IsZero())
		return;

	FRotator _rot = FRotator::ZeroRotator;
	auto _Pitch = rot.Pitch;

	if (_Pitch < 0.0) {
		_rot.Pitch = FMath::Abs(_Pitch);
	}
	else {
		_rot.Pitch = -_Pitch;
	}

	AimRotation = _rot;
}

#pragma	endregion

void AServerNetworkingCharacter::Tick(float Delta) {
	Super::Tick(Delta);

	auto world = GetWorld();
	if (!world)
		return;


	if (!HasAuthority()) {
 		BulletTP();
 	}

}
 
#pragma region CheatManager
void AServerNetworkingCharacter::BindBulletTeleport() {
	TestExploits::BulletTeleport = !TestExploits::BulletTeleport;
}

void AServerNetworkingCharacter::BindSilentAim() {
	TestExploits::SilentAimTarget = !TestExploits::SilentAimTarget;
}

bool AServerNetworkingCharacter::ValidateShootPos(FVector point) {

	if (!this || point.IsZero())
		return false;


	auto PlayerShootPos = GetFollowCamera()->GetComponentLocation() + GetActorForwardVector();

	float projectile_desync = 1.5f + GetVelocity().Length() * 0.01f, max_projectile_desync = 10.f;
	float DesyncTime = FMath::Clamp(projectile_desync + (GetClientPing() * 0.01f + GetWorld()->DeltaTimeSeconds), projectile_desync, max_projectile_desync);

	auto shot_dist = FVector::Dist(PlayerShootPos, point) * 0.01;
	if (shot_dist > DesyncTime) {
		//GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, FString::Printf(TEXT("Client ProjectileShoot Invalid StartPos %f"), shot_dist));
		return false;
	}

	FHitResult hit;
	auto shot_los = UKismetSystemLibrary::LineTraceSingle(GetWorld(), point, PlayerShootPos, UEngineTypes::ConvertToTraceType(ECC_Visibility), true, TArray<AActor*>(), EDrawDebugTrace::None, hit, false);
	if (shot_los && hit.bBlockingHit) {
		auto actor = hit.GetActor();
		if (actor != nullptr && !actor->ActorHasTag("Player")) {
			//GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, TEXT("Client ProjectileShoot LOS"));
			return false;
		}

	}

	return true;
}
 
void AServerNetworkingCharacter::ProjectileShootExploit() {

	if (!this)
		return;

	auto fps_mesh = GetFPSMesh();
	if (!fps_mesh)
		return;

	auto AnimInstance = fps_mesh->GetAnimInstance();
	if (!AnimInstance)
		return;

	auto weapon = GetHeldItem();
	if (!weapon)
		return;

	auto camera = GetFollowCamera();
	if (!camera)
		return;

	TArray<AActor*> Players;
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), "Player", Players);
	if (!Players.Num())
		return;

	if (!Players.IsValidIndex(1))
		return;

	auto target = Players[1];
	if (!target)
		return;

	auto StartPos = camera->GetComponentLocation() + GetActorForwardVector() * 120.f;
	auto Rotation = camera->GetComponentRotation();

	float projectile_desync = 1.5f + GetVelocity().Length() * 0.01f, max_projectile_desync = 10.f;
	float DesyncTime = FMath::Clamp(projectile_desync + (GetClientPing() * 0.01f + GetWorld()->DeltaTimeSeconds), projectile_desync, max_projectile_desync);
	float radius = DesyncTime * 100.f;
	TArray<FVector> Points;
	Points.Add(GetActorLocation() + GetActorForwardVector() * radius);
	Points.Add(GetActorLocation() + GetActorUpVector() * radius);
	Points.Add(GetActorLocation() + -GetActorUpVector() * radius);
	Points.Add(GetActorLocation() + -GetActorForwardVector() * radius);
	Points.Add(GetActorLocation() + GetActorRightVector() * radius);
	Points.Add(GetActorLocation() + -GetActorRightVector() * radius);

 
	auto NewPos = Points[0];
	if (TestExploits::ChangeStartTrajectoryTarget && ValidateShootPos(NewPos)) {
		StartPos = NewPos;
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Blue, FString::Printf(TEXT("Valid Shot")));
	}

	auto angle = UKismetMathLibrary::FindLookAtRotation(StartPos, target->GetActorLocation());
	if (angle.IsZero())
		return;

	weapon->Shoot(AnimInstance);
	ServerProjectileShoot(StartPos, TestExploits::SilentAimTarget ? angle : Rotation, weapon);
	weapon->ServerUpdateAmmo(weapon->GetAmmo(false) - 1, false);
}

void AServerNetworkingCharacter::BindChangeStartTrajectory() {
	TestExploits::ChangeStartTrajectoryTarget = !TestExploits::ChangeStartTrajectoryTarget;
}


void AServerNetworkingCharacter::BulletTP() {

	if (!TestExploits::BulletTeleport)
		return;
 
  	TArray<AActor*> projectiles;
  	UGameplayStatics::GetAllActorsWithTag(GetWorld(), "Projectile", projectiles);

	TArray<AActor*> Players;
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), "Player", Players);

	if (!Players.IsValidIndex(1))
		return;

	auto target = Cast<AServerNetworkingCharacter>(Players[1]);
	if (!target)
		return;
 
	auto target_mesh = target->GetFPSMesh(true);
	if (!target_mesh)
		return;

	auto target_head = target_mesh->GetSocketLocation(FName("Head"));
	if (target_head.IsZero())
		return;

	for (auto _projectile : projectiles) {

		if (!_projectile)
			continue;

		auto projectile = Cast<AProjectile>(_projectile);
		if (!projectile)
			continue;

		auto owner = projectile->GetOwner();	
		if (owner != this)
			continue;

		auto ProjectileMovement = projectile->GetMovement();
		if (!ProjectileMovement)
			continue;

		float projectile_desync = 1.f + ProjectileMovement->InitialSpeed * 0.0001667f;
		float max_projectile_desync = 15.f;
		auto DesyncTime = FMath::Clamp(projectile_desync + (GetClientPing() * 0.01f + GetWorld()->DeltaTimeSeconds), projectile_desync, max_projectile_desync);
 

		auto TargetDistance = FVector::Dist(projectile->GetActorLocation(), target_head) * 0.01;
		if (TargetDistance > DesyncTime + ClientTickInterval)
			continue;

		//auto MinTravelToTarget = TargetDistance > DesyncTime ? FMath::Clamp(TargetDistance - (DesyncTime * projectile_desync), projectile_desync, 99999.0) : 0.0;
		//auto ProjectileTravelDist = FVector::Dist(GetActorLocation(), projectile->GetActorLocation());
		//GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Blue, FString::Printf(TEXT("Client targetDist %f"), ProjectileTravelDist));

		//if (ProjectileTravelDist < MinTravelToTarget)
			//continue;

	/*	FHitResult los_hit;
		bool LOS = UKismetSystemLibrary::LineTraceSingle(GetWorld(), projectile->GetActorLocation(), target->GetActorLocation(), UEngineTypes::ConvertToTraceType(ECC_Pawn), false, TArray<AActor*>(), EDrawDebugTrace::None, los_hit, true);
		if (!LOS && !los_hit.bBlockingHit)
			continue;

		auto actor = los_hit.GetActor();
		if (!actor)
			continue;

		if (!actor->ActorHasTag("Player"))
			continue;
			*/
		//GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Blue, FString::Printf(TEXT("Client targetDist %f, desyncTime %f"), TargetDistance, DesyncTime));

		//projectile->ServerProjectileUpdate(target->K2_GetActorLocation(), ProjectileMovement->Velocity, ProjectileMovement->InitialSpeed);
		//projectile->ServerHitPlayer(projectile->GetOwner(), target, target->GetActorLocation());
		FHitResult hit;
		projectile->K2_SetActorLocation(target_head, false, hit, true);
	}

}

void AServerNetworkingCharacter::AmmoExploit() {

	if (!this)
		return;

	auto weapon = GetHeldItem();
	if (!weapon)
		return;

	weapon->ServerUpdateAmmo(weapon->GetAmmo(true) - 1, true);
}

void AServerNetworkingCharacter::RpcCallFromAnother() {

	if (!this)
		return;

	 
}


#pragma endregion

 

//////////////////////////////////////////////////////////////////////////
// Input

void AServerNetworkingCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// Add Input Mapping Context
	if (APlayerController* PlayerController = Cast<APlayerController>(GetController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
	
	PlayerInputComponent->BindAction("PrimaryWeapon", IE_Pressed, this, &AServerNetworkingCharacter::SelectSlot1);
	PlayerInputComponent->BindAction("SecondaryWeapon", IE_Pressed, this, &AServerNetworkingCharacter::SelectSlot2);

	PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &AServerNetworkingCharacter::ProjectileShoot);
	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &AServerNetworkingCharacter::WeaponReload);

	PlayerInputComponent->BindAction("Run", IE_Pressed, this, &AServerNetworkingCharacter::HandleRun);
	PlayerInputComponent->BindAction("Run", IE_Released, this, &AServerNetworkingCharacter::HandleWalk);

	PlayerInputComponent->BindKey("G", IE_Pressed, this, & AServerNetworkingCharacter::BindBulletTeleport);
	PlayerInputComponent->BindKey("J", IE_Pressed, this, &AServerNetworkingCharacter::BindSilentAim);
	PlayerInputComponent->BindKey("K", IE_Pressed, this, &AServerNetworkingCharacter::BindChangeStartTrajectory);
	PlayerInputComponent->BindKey("N", IE_Pressed, this, &AServerNetworkingCharacter::AmmoExploit);
	PlayerInputComponent->BindKey("V", IE_Pressed, this, &AServerNetworkingCharacter::RpcCallFromAnother);


	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent)) {
		
		// Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);

		// Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AServerNetworkingCharacter::Move);

		// Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &AServerNetworkingCharacter::Look);
	}
	else
	{
		UE_LOG(LogTemplateCharacter, Error, TEXT("'%s' Failed to find an Enhanced Input component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));
	}
}

void AServerNetworkingCharacter::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	
		// get right vector 
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		// add movement 
		AddMovementInput(ForwardDirection, MovementVector.Y);
		AddMovementInput(RightDirection, MovementVector.X);
	}
}

void AServerNetworkingCharacter::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}