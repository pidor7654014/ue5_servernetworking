// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Logging/LogMacros.h"
#include "../PlayerInventory/PlayerInventory.h"
#include "ServerNetworkingCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UInputMappingContext;
class UInputAction;
struct FInputActionValue;

DECLARE_LOG_CATEGORY_EXTERN(LogTemplateCharacter, Log, All);

UCLASS(Blueprintable)
class AServerNetworkingCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FollowCamera;
	
	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputMappingContext* DefaultMappingContext;

	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* JumpAction;

	/** Move Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* MoveAction;

	/** Look Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* LookAction;


public:
	AServerNetworkingCharacter();
	
	UPROPERTY(Replicated)
	bool SprintPressed;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly)
	float Health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ClientTickInterval;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category="MainPlayer")
	UPlayerInventory* Inventory;

	
	UFUNCTION(BlueprintCallable, Category="MainPlayer")
	ABaseWeapon* GetHeldItem();

	UFUNCTION(BlueprintCallable, Category = "MainPlayer")
	FString GetHeldItemName();

	UFUNCTION(BlueprintCallable, Category = "MainPlayer")
	int GetWeaponAmmo(bool max);

	UFUNCTION(BlueprintCallable, Category = "MainPlayer")
	int GetInventoryAmmo();

	UFUNCTION(BlueprintCallable, Category = "MainPlayer")
	float GetClientPing(bool realValue = false);
 
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly)
	bool IsReloading;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly)
	FRotator AimRotation;


 protected:

 
	 void AmmoExploit();

	 void BindBulletTeleport();
	 void BulletTP();

	 void BindSilentAim();
	 void BindChangeStartTrajectory();

	 bool ValidateShootPos(FVector point);
 	 void ProjectileShootExploit();

	USkeletalMeshComponent* GetFPSMesh(bool another = false);

	void RpcCallFromAnother();
	
	void SelectSlot1();
	void SelectSlot2();

	void HandleWalk();
	void HandleRun();

	void ProjectileShoot();
	void WeaponReload();

	void SendPlayerTick();

	void ClearReload();

	void HideThirdPersonWeapon();

	UFUNCTION(Server, Reliable)
	void ServerSendSprintInput(bool value);
	void ServerSendSprintInput_Implementation(bool value);

 
	UFUNCTION(Server, Reliable)
	void ServerSendPlayerTick();
	void ServerSendPlayerTick_Implementation();

	UFUNCTION(Server, Reliable)
	void ServerProjectileShoot(FVector Start, FRotator Rotation, ABaseWeapon* weapon);
	void ServerProjectileShoot_Implementation(FVector Start, FRotator Rotation, ABaseWeapon* weapon);

	UFUNCTION(NetMulticast, Reliable)
	void MultiCastProjectileShoot(FVector Start, FRotator Rotation, ABaseWeapon* weapon);
	void MultiCastProjectileShoot_Implementation(FVector Start, FRotator Rotation, ABaseWeapon* weapon);

	UFUNCTION(Server, Reliable)
	void ServerUpdateSpeed(float speed);
	void ServerUpdateSpeed_Implementation(float speed);

	UFUNCTION(NetMulticast, Reliable)
	void MultiCastUpdateSpeed(float speed);
	void MultiCastUpdateSpeed_Implementation(float speed);


	UFUNCTION(NetMulticast, Reliable)
	void MultiCastUpdatePosition(FVector Location);
	void MultiCastUpdatePosition_Implementation(FVector Location);


	UFUNCTION(Server, Reliable)
	void ServerUpdateWeapon(bool PrimarySlot);
	void ServerUpdateWeapon_Implementation(bool PrimarySlot);

	UFUNCTION(Client, Reliable)
	void ClientUpdateWeapon(bool PrimarySlot);
	void ClientUpdateWeapon_Implementation(bool PrimarySlot);

	UFUNCTION(NetMulticast, Reliable)
	void MultiCastUpdateWeapon(bool PrimarySlot);
	void MultiCastUpdateWeapon_Implementation(bool PrimarySlot);

	UFUNCTION(Server, Reliable)
	void ServerSimulateReload(ABaseWeapon* weapon, int AmmoClipSize);
	void ServerSimulateReload_Implementation(ABaseWeapon* weapon, int AmmoClipSize);

	UFUNCTION(Server, Reliable)
	void ServerSetPlayerReloading(bool value);
	void ServerSetPlayerReloading_Implementation(bool value);

	UFUNCTION(NetMulticast, Reliable)
	void MultiCastSetPlayerReloading(bool value);
	void MultiCastSetPlayerReloading_Implementation(bool value);


	UFUNCTION(Server, Reliable)
	void ServerUpdateAimRotation(FRotator rot);
	void ServerUpdateAimRotation_Implementation(FRotator rot);

	UFUNCTION(NetMulticast, Reliable)
	void MultiCastUpdateAimRotation(FRotator rot);
	void MultiCastUpdateAimRotation_Implementation(FRotator rot);


	/** Called for movement input */
	void Move(const FInputActionValue& Value);

	/** Called for looking input */
	void Look(const FInputActionValue& Value);
			
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	// To add mapping context
	virtual void BeginPlay();
	virtual void Tick(float Delta);

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};

