// Fill out your copyright notice in the Description page of Project Settings.


#include "../PlayerInventory/PlayerInventory.h"
#include "Kismet/GameplayStatics.h"
#include <Net/UnrealNetwork.h>
#include "../Utils/ServerSideObject.h"

UPlayerInventory::UPlayerInventory()
{

	PistolAmmo = 128;
	RifleAmmo = 128;
	SelectedWeapon = nullptr;
	InventoryList = TArray<ABaseWeapon*>();
}

UPlayerInventory::~UPlayerInventory()
{

}

bool UPlayerInventory::CanAddItem(ABaseWeapon* weapon) {

	if (!weapon)
		return false;

	for (auto item : InventoryList) {

		if (!item)
			continue;

		if (item->GetWeaponName().Equals(weapon->GetWeaponName())) {
			return false;
		}

	}

	return true;
}


void UPlayerInventory::Initialize(USkeletalMeshComponent* attachTo, FString object, AActor* Player) {

	if (!this || !attachTo) {
		UE_LOG(LogTemp, Warning, TEXT("ptr or attach comp is null"));
		return;
	}

	auto world = GetWorld();
	if (!world)
		return;

	auto obj = StaticLoadClass(ABaseWeapon::StaticClass(), nullptr, *object);
	if (!obj) {
		UE_LOG(LogTemp, Warning, TEXT("failed load object %s"), *object);
		return;
	}

	auto weapon = world->SpawnActor<ABaseWeapon>(obj, attachTo->GetRelativeLocation(), attachTo->GetRelativeRotation());
	if (!weapon)
		return;

	if (weapon->GetSocket().IsNone())
		return;

	if (!CanAddItem(weapon))
		return;

	if (!weapon->AttachToComponent(attachTo, FAttachmentTransformRules::SnapToTargetIncludingScale, weapon->GetSocket())) {
		weapon->Destroy();
		UE_LOG(LogTemp, Warning, TEXT("failed attach gun to mesh"));
		return;
	}

 	weapon->SetHidden(true);
	InventoryList.Add(weapon);
	weapon->ServerPushWeapon(Player);
}

void UPlayerInventory::ServerInitializeAmmo_Implementation(AServerNetworkingCharacter* _instigator) {

	if (!this)
		return;

	auto ServerObj = Cast<AServerSideObject>(UGameplayStatics::GetActorOfClass(GetWorld(), AServerSideObject::StaticClass()));
	if (!ServerObj)
		return;

	auto PlayerUpdates = ServerObj->FindPlayerUpdates(_instigator);
	if (!PlayerUpdates)
		return;

	const int maxSize = 2;
	int size = PlayerUpdates->InventoryAmmo.Num();
	if (size > maxSize)
		return;

	PlayerUpdates->InventoryAmmo.Add(0, 128);
	PlayerUpdates->InventoryAmmo.Add(1, 128);
}


int UPlayerInventory::GetWeaponListSize() {

	if (!this)
		return 0;

	return InventoryList.Num();
}

ABaseWeapon* UPlayerInventory::GetWeaponByIndex(int id) {
	
	if (!this || id > InventoryList.Num())
		return nullptr;

	return InventoryList[id];
}

ABaseWeapon* UPlayerInventory::GetWeapon() {
	
	if (!this)
		return nullptr;
	
	return SelectedWeapon;
}


void UPlayerInventory::SetAmmo(int value) {

	if (!this || !SelectedWeapon)
		return;

	int ammo = 0;
	switch (SelectedWeapon->GetAmmoType()) {
	case 0:
		PistolAmmo = value;
		break;
	case 1:
		RifleAmmo = value;
		break;
	}
}

int UPlayerInventory::GetAmmo() {

	if (!this || !SelectedWeapon)
		return 0;

	int ammo = 0;
	switch (SelectedWeapon->GetAmmoType()) {
	case 0:
		ammo = PistolAmmo;
		break;
	case 1:
		ammo = RifleAmmo;
		break;
	}

	return ammo;
}

ABaseWeapon* UPlayerInventory::GetCurrentSlotWeapon(bool Primary) {

	if (!this || !InventoryList.Num())
		return nullptr;
	
	for (auto item : InventoryList) {

		if (!item)
			continue;

		if (Primary && item->IsPrimary())
			return item;
		else if (!Primary && !item->IsPrimary())
			return item;

	}

	return nullptr;
}

void UPlayerInventory::SelectWeapon(ABaseWeapon* weapon, USkeletalMeshComponent* mesh, bool re_updateMesh) {

	if (!this || !mesh)
		return;

	if (!re_updateMesh) {
		if (SelectedWeapon != nullptr)
			SelectedWeapon->SetHidden(true);

		weapon->SetHidden(false);
		SelectedWeapon = weapon;
	}

	if (!SelectedWeapon->AttachToComponent(mesh, FAttachmentTransformRules::SnapToTargetIncludingScale, SelectedWeapon->GetSocket())) {
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, TEXT("failed attach"));
	}
}
 
 