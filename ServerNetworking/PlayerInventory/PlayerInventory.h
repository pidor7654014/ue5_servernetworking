// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Weapon/BaseWeapon.h"
#include "PlayerInventory.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class UPlayerInventory : public UObject
{
	GENERATED_BODY()

public:
	UPlayerInventory();
	~UPlayerInventory();

	 
 	void Initialize(USkeletalMeshComponent* attachTo, FString object, AActor* Player);

	UFUNCTION(Server, Reliable)
	void ServerInitializeAmmo(AServerNetworkingCharacter* _instigator);
	void ServerInitializeAmmo_Implementation(AServerNetworkingCharacter* _instigator);

	void SetAmmo(int value);
	int GetAmmo();
	int GetWeaponListSize();

	ABaseWeapon* GetWeaponByIndex(int id);
	ABaseWeapon* GetWeapon();
	ABaseWeapon* GetCurrentSlotWeapon(bool Primary);
 	void SelectWeapon(ABaseWeapon* weapon, USkeletalMeshComponent* mesh, bool re_updateMesh);

protected:

	bool CanAddItem(ABaseWeapon* weapon);

 	int PistolAmmo;
	int RifleAmmo;

	TArray<ABaseWeapon*> InventoryList;
	ABaseWeapon* SelectedWeapon;
 };
